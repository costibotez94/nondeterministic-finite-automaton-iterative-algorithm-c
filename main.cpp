#include<string>
#include<vector>
#include<iostream>
#include<conio.h>

using namespace std;

class Tranzitie{
    private:
        string stare_inceput;
        char simbol;
        string stare_sfarsit;
    public:
        Tranzitie(string stare_inceput,char simbol,string stare_sfarsit)
        {
          (*this).stare_inceput=stare_inceput;
          (*this).simbol=simbol;
          (*this).stare_sfarsit=stare_sfarsit;
        }
     string spuneStareInceput(){return(*this).stare_inceput;}
     char spuneSimbol(){return(*this).simbol;}
     string spuneStareSfarsit(){return(*this).stare_sfarsit;}
};

 class ListaTranzitii{
   private:
        vector<Tranzitie>lista;
   public:
        ListaTranzitii(){}
        void adaugaTranzitie(Tranzitie tr){lista.push_back(tr);}
        string gasesteStareUrmatoare(string stare,char simbol);
        int getSize();
        Tranzitie getTranzitie(int poz);
};

string ListaTranzitii::gasesteStareUrmatoare(string stare,char simbol)
{
    int i=(*this).lista.size();
    for(int j=0;j<i;j++)
    {
            Tranzitie tmp=(*this).lista.at(j);
            if(tmp.spuneStareInceput()==stare&&tmp.spuneSimbol()==simbol)
                return tmp.spuneStareSfarsit();
    }
    return "0";
}

int ListaTranzitii::getSize()
{
    return (*this).lista.size();
}

Tranzitie ListaTranzitii::getTranzitie(int poz )
{
    return (*this).lista.at(poz);
}

class Automat{
    private:
        string stare_initiala;
        ListaTranzitii lista;
        vector<string>stari_finale;
    public:
        Automat(string stare_initiala,ListaTranzitii lista,vector<string> stari_finale)
        {
            (*this).stare_initiala=stare_initiala;
            (*this).lista=lista;
            (*this).stari_finale=stari_finale;
        }

    void tiparesteAutomat();

    int analizeaza(string sir_intrare)
    {
        string stare_actuala = stare_initiala;
        for (int i = 0; i < sir_intrare.size(); i++)
        {
            cout<<"st "<<stare_actuala<<endl;
            if (lista.gasesteStareUrmatoare(stare_actuala, sir_intrare[i]) != "0")
            {
                stare_actuala = lista.gasesteStareUrmatoare(stare_actuala, sir_intrare[i]);
            }
            else
                return 0;
        }

        for (int i = 0; i < (*this).stari_finale.size(); i++)
            if (stare_actuala==(*this).stari_finale[i])
                return 1;

        return 0;
    };
};

void Automat::tiparesteAutomat()
{
     cout<<"stare initiala:"<<stare_initiala<<endl;
     for(int i=0;i<stari_finale.size();i++)
        cout<<"stare finala"<<stari_finale.at(i)<<endl;
}


int main()
{
    string cuv;
    Tranzitie *t1=new Tranzitie("q0",'a',"q1");
    Tranzitie *t2=new Tranzitie("q1",'b',"q1");
    Tranzitie *t3=new Tranzitie("q1",'a',"q2");
    /*cout<<"tranzitie t1"<<(*t1).spuneStareInceput()<<(*t1).spuneSimbol()<<(*t1).spuneStareSfarsit();*/

    ListaTranzitii*lista=new ListaTranzitii();
    (*lista).adaugaTranzitie(*t1);
    (*lista).adaugaTranzitie(*t2);
    (*lista).adaugaTranzitie(*t3);
    vector<string> starifinale;
    starifinale.push_back("q1");
    starifinale.push_back("q2");
    Automat *M=new Automat("q0",*lista,starifinale);
    (*M).tiparesteAutomat();

    cout<<"Introduceti cuvantul: ";
    cin>>cuv;
    if((*M).analizeaza(cuv)==1)
        cout<<"Cuvantul apartine automatului";
    else
        cout<<"Cuvantul nu apartine automatului";
    getch();

    return 1;
}
